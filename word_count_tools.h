#include <string>
#include <unordered_map>
#include <ostream>
#include <vector>

void do_and_print_freq(std::ostream& output, std::unordered_map<std::string, int> counts);
void do_and_print_freq_decompressed(std::ostream& output, std::unordered_map<std::string, int> counts);
std::unordered_map<std::string, int> count_words_in_range(const char* input_begin, const char* input_end);
std::unordered_map<std::string, int> count_compressed_words_in_range(const char* input_begin, const char* input_end);
std::unordered_map<std::string, int> merge_counts(std::vector<std::unordered_map<std::string, int>> counts_vec);
