#include <iostream>
#include <vector>
#include <future>
#include <numeric>
#include <unordered_map>
#include "mmap_file_reader.h"
#include "word_count_tools.h"
#include <cstdio>
#include <unistd.h>
#include <fstream>
#include "string_tools.h"

template<typename Adr, class IsSep>
std::vector<std::pair<Adr, Adr>> split_range_on_separators(Adr input_begin, Adr input_end, int chunks_count_expected, IsSep is_sep, int max_adjust) {
    size_t input_len = std::distance(input_begin, input_end);
    size_t avg_chunk_size = input_len / chunks_count_expected;

    
    std::vector<std::pair<Adr, Adr>> chunks;
    
    for (Adr b = input_begin; b != input_end; ) {
        auto e = b + avg_chunk_size;
        if (e > input_end) {
            e = input_end;
        } else {
            int ajust_count = 0;
            while (ajust_count != max_adjust && e != input_end && !is_sep(*e)) {
                ++e;
                ++ajust_count;
            }
            if (ajust_count == max_adjust) {
                throw std::runtime_error(std::string("Unable to split the range, too long segments without a separator. No separator during ") + std::to_string(max_adjust) + "chars.");
            }
        }
        chunks.emplace_back(b, e);
        b = e;
    }

    return chunks;
}


int main(int argc, const char* argv[]) {
    if (argc < 4) {
        std::cerr << "Usage: ./async_test  input_file output_file threads_number" << std::endl;
        return 1;
    }
    MMapedFileReader reader(argv[1]);
    std::ofstream output(argv[2]);
    int thread_number = std::stoi(argv[3]);
    int max_allowed_word_len = 128;
    

    std::vector<std::future<std::unordered_map<std::string, int>>> word_counts_in_chunks_futures;
    
    auto chunks = split_range_on_separators(reader.get_start(), reader.get_start() + reader.get_len(), thread_number, [](char c) {return !std::isalpha(c);}, max_allowed_word_len);

    for (auto be : chunks) {
        word_counts_in_chunks_futures.push_back(std::async(std::launch::async, [=]() {
            return count_compressed_words_in_range(be.first, be.second);
            return count_words_in_range(be.first, be.second);
        }));
        
    }

    std::vector<std::unordered_map<std::string, int>> counts_vec;
    for (auto& f : word_counts_in_chunks_futures) {
        counts_vec.push_back(f.get());
    }
    
    auto ans = merge_counts(std::move(counts_vec));

    do_and_print_freq_decompressed(output, ans);
    do_and_print_freq(output, ans);

    return 0;
}
