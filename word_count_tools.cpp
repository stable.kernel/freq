#include "word_count_tools.h"
#include <map>
#include <functional>
#include <algorithm>
#include "string_tools.h"

void do_and_print_freq(std::ostream& output, std::unordered_map<std::string, int> counts) {
    std::map<int, std::vector<std::string>, std::greater<int>> freq;
    
    for (auto& wc: std::move(counts)) {
        freq[wc.second].push_back(std::move(wc.first));
    }

    for (auto& c_words : std::move(freq)) {
        auto& words = c_words.second;
        std::sort(words.begin(), words.end());
        for (const auto& w : words) {
            output << c_words.first << " " << w << std::endl;
        }
    }
}

void do_and_print_freq_decompressed(std::ostream& output, std::unordered_map<std::string, int> counts_compressed) {
    std::map<int, std::vector<std::string>, std::greater<int>> freq;
    
    std::string decompressed;
    for (auto& wc: std::move(counts_compressed)) {
        decompress(wc.first, decompressed);
       freq[wc.second].push_back(decompressed);
    }

    for (auto& c_words : std::move(freq)) {
        auto& words = c_words.second;
        std::sort(words.begin(), words.end());
        for (const auto& w : words) {
            output << c_words.first << " " << w << std::endl;
        }
    }
}

std::unordered_map<std::string, int> count_words_in_range(const char* input_begin, const char* input_end) {
    std::unordered_map<std::string, int> counts;
    std::string word;
    for (const char* p = input_begin; p != input_end; ++p) {
        char c = *p;
        if ('a' <= c && c <= 'z') {
            word += c;

        } else if('A'  <= c && c <= 'Z') {
            word += c + ('a' - 'A');          
        } else {
            if (!word.empty()) {
                counts[word]++;
                word.clear();
            }
        }
    }
    if (!word.empty()) {
        counts[std::move(word)]++;
    }
    return counts;
}

std::unordered_map<std::string, int> count_compressed_words_in_range(const char* input_begin, const char* input_end) {
    std::unordered_map<std::string, int> counts;
    std::string word;
    std::string compressed_word;
    for (const char* p = input_begin; p != input_end; ++p) {
        char c = *p;
        if ('a' <= c && c <= 'z') {
            word += c;

        } else if('A'  <= c && c <= 'Z') {
            word += c + ('a' - 'A');          
        } else {
            if (!word.empty()) {
                compress(word, compressed_word);
                counts[compressed_word]++;
                word.clear();
            }
        }
    }
    if (!word.empty()) {
        compress(word, compressed_word);
        counts[std::move(compressed_word)]++;
    }
    return counts;
}

std::unordered_map<std::string, int> merge_counts(std::vector<std::unordered_map<std::string, int>> counts_vec) {
    auto counts = std::move(counts_vec[0]);
    for (size_t i = 1; i < counts_vec.size(); ++i) {
        for (auto wc : std::move(counts_vec[i])) {
            counts[std::move(wc.first)] += wc.second;
        }
    }

    return counts;
}
