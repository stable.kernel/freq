#include <fstream>
#include <random>
#include <iostream>
#include <cassert>
#include <sstream>
#include <unordered_set>
#include <vector>

void gen_random(std::ostream& output, size_t wanted_len) {
    std::string chars;
    chars += "       \n +_()";
    for (char c  = 'A'; c <= 'Z'; ++c) {
        chars += c;
    }
    for (char c  = 'a'; c <= 'z'; ++c) {
        chars += c;
    }

    std::random_device rd;  // a seed source for the random number engine
    std::mt19937 gen(rd()); // mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<size_t> distrib(0, chars.size() - 1);

    for (size_t i = 0; i != wanted_len; ++i) {
        output << chars[distrib(gen)];
    }
    
}

void gen_from_fixed_vocabulary(std::ostream& output, size_t wanted_len) {
    std::string seps("\n,,33323423412349                 80192804--=-=-=-=-=-2410");
    std::vector<std::string> words;
    int max_word_len = 40;
    words.push_back("a");
    for (int i = 0; i != max_word_len; ++i) {
        words.push_back(words.back() + "A");
    }

    words.push_back("B");
    for (int i = 0; i != max_word_len; ++i) {
        words.push_back(words.back() + "b");
    }

    std::random_device rd;  // a seed source for the random number engine
    std::mt19937 gen(rd()); // mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<size_t> distrib(0, words.size() - 1);


    
    size_t len = 0;
    while (len < wanted_len) {
        std::string sep( "1 2");
        sep += seps[distrib(gen) % seps.size()];
        size_t w_i = distrib(gen);
        size_t w_len = words[w_i].size();
        size_t rest = wanted_len - len;
        if (rest >= w_len) {
            output<< words[w_i];
            
        } else {
            output << std::string(rest, '.');
            len += rest;
            break;
        }
        len += w_len;
        rest = wanted_len - len; 
        if ( rest >= sep.size()) {
            output << sep;
            len += sep.size();
        } else {
            output << std::string(rest, '=');
            len += rest;
            break;
        }
    }
}

const char* USAGE = "Usage ./gen method output_size\n Method=random|dict\n You can use 'k', 'm' and 'g' suffixes for size number.";
int main(int argc, char* argv[]) {

    if (argc < 2) {
        std::cerr << USAGE  << std::endl;
        return 1;
    }
    std::stringstream ss(argv[2]);
    size_t total_wanted_len = 0;
    ss >> total_wanted_len;
    char c;
    while (ss >> c) {
        if (std::tolower(c) == 'k') {
            total_wanted_len *= 1024;
        } else if (std::tolower(c) == 'm') {
            total_wanted_len *= 1024 * 1024;
        } else if (std::tolower(c) == 'g') {
            total_wanted_len *= 1024 * 1024 * 1024;
        } else {
            std::cerr << "Error: Unrecognized: output_size.\n" << USAGE << std::endl;
        }
    }

    if (std::string("random") == argv[1]) {
        gen_random(std::cout, total_wanted_len);
    } else if (std::string("dict") == argv[1]) {
        gen_from_fixed_vocabulary(std::cout, total_wanted_len);
    } else {
        throw std::runtime_error(std::string("unknown method: ") + argv[1]);
    }

}

// void gen_long_words(std::ostream& output, size_t wanted_len, std::random_device& rd) {
//     std::string word;
//     std::unordered_set<std::string> words;

