#include <string>

void compress(const std::string& str, std::string& to);
void decompress(const std::string& coded, std::string& to);