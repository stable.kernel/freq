#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <algorithm>
#include <sys/mman.h>
#include <string_view>
#include <memory>

class MMapedFileReader {
    // Optional<struct stat> stats;
    // Optional<char*> start;

    std::unique_ptr<struct stat> stats;
    std::unique_ptr<char*> start;
    int fd = -33;
public:
    MMapedFileReader(const char* fname) {
        fd = open(fname, O_RDONLY);
        if (fd < 0) {
            throw std::runtime_error(std::string("Failed to open the file for reading <") + fname+ '>');
        }

        struct stat stats_loc;
        if (fstat(fd, &stats_loc) < 0) {
            throw std::runtime_error(std::string("Failed to get file size of <") + fname + '>');          
        }

        stats = std::make_unique<struct stat>(std::move(stats_loc));

        off_t fileSize = stats_loc.st_size;
      
        char* fileData = static_cast<char*>(mmap(NULL, fileSize, PROT_READ, MAP_PRIVATE, fd, 0));
        if (fileData == MAP_FAILED) {
            throw std::runtime_error(std::string("Failed to memory-map the file.") + fname + '>');
        }

        start = std::make_unique<char*>(fileData);
    }

    const char* get_start() const {
        return *start;
    }

    size_t get_len() const {
        return stats->st_size;
    }

    ~MMapedFileReader() {
        if (start) {
            munmap(*start, stats->st_size);
        }
        close(fd);
    }
};