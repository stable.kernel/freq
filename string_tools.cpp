#include <string>

// std::pair<char, char> decompress_char(char c) {
//     return {(c & 0b11110000) >> 4, c & 0b00001111}
// }

void compress(const std::string& str, std::string& to) {
    to.clear();

    for (int i = 0; i + 1 < str.size(); i += 2) {
        char a = str[i];
        char b = str[i + 1];

        to += ((a - 'a') << 4) | (b - 'a');
    }

    if (str.size() & 1) {
        to += ((str.back() - 'a') << 4) | ('z' - 'a' + 1);
    }
}

void decompress(const std::string& coded, std::string& to) {
    to.clear();
    constexpr char z_coded = 'z' - 'a';
    for (char c : coded) {
        to += ((c & 0b11110000) >> 4) + 'a';
        char b = c & 0b00001111;
        if (b <= z_coded) {
            to += b + 'a';
        }
    }
}